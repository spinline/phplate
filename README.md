# PHPlate

Boilerplate based on [generator webapp](https://github.com/yeoman/generator-webapp)

## Features

- enable [ES2015 features](https://babeljs.io/docs/learn-es2015/) using [Babel](https://babeljs.io)
- CSS Autoprefixing
- Built-in preview server with BrowserSync
- Automagically compile Sass with [libsass](http://libsass.org)
- Automagically lint your scripts
- Map compiled CSS to source stylesheets with source maps
- Awesome image optimization
- Automagically wire-up dependencies installed with [Bower](http://bower.io)

## Included Options
- Sass
- Bootstrap
- Modernizr

## What’s different compared to **generator webapp**?

- Serve PHP files, too [[Reference]](http://stackoverflow.com/questions/27990781/gulp-webapp-running-browsersync-and-php)
- SVGs can be compiled / used as `<symbol>`s [[Reference]](https://css-tricks.com/svg-symbol-good-choice-icons/)
- Helpful CSS error notifications on the desktop
- This is not a generator… yet [TODO]

## Getting Started

### Don’t already have [Gulp](http://gulpjs.com/) and [Bower](https://bower.io/) already installed? 

Run: `npm install --global gulp-cli bower`

### Next steps…

- Run `npm install` to install npm packages
- Run `bower install` to install bower components
- Run `gulp php-serve` to preview and watch for changes
- Run `bower install --save <package>` to install frontend dependencies
- Run `gulp serve:test` to run the tests in the browser
- Run `gulp to build` your webapp for production
- Run `gulp serve:dist` to preview the production build
- Run `npm install --save <package>` to install additional npm packages